package us.d8u.finance;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImportFromMail implements ImportFrom {
	private static Logger log = LoggerFactory.getLogger(ImportFromMail.class);
	private static final Properties configuration = new Properties();
	private Message message;

	private static void loadConfig() {
		try {
			InputStream configSrc = ImportFromMail.class.getClassLoader()
					.getResourceAsStream("FinanceOCR.properties");
			configuration.load(configSrc);
		} catch (IOException e) {
			log.warn(e.getLocalizedMessage());
		}
	}

	public ImportFromMail(Message aMessage) {
		if (configuration.getProperty("loaded") == null) {
			loadConfig();
		}
		message = aMessage;
	}

	public ReceiptBean toReceiptBean() {
		ReceiptBean ret = new ReceiptBean();
		String[] subjectParts = null;
		try {
			ret.setDoneAt(message.getSentDate());
			subjectParts = message.getSubject().split(" ");
		} catch (MessagingException m) {
			log.warn(m.getLocalizedMessage());
		}
		try {
			double amount = Double
					.parseDouble(subjectParts[subjectParts.length - 1]);
			ret.setAmount(amount);
		} catch (NumberFormatException e) { // not a raw number, try a currency
			// before barfing
			try {
				NumberFormat.getCurrencyInstance().parseObject(
						subjectParts[subjectParts.length - 1]);
			} catch (ParseException e1) {
				log.warn(subjectParts[subjectParts.length - 1] + ", "
						+ e1.getLocalizedMessage());
			}
		}
		String location = "";
		for (String part : subjectParts) {
			try {
				Double.parseDouble(part);
				continue;
			} catch (NumberFormatException e) {
				log.warn(e.getLocalizedMessage());
			}

			InternetAddress from = null;
			try {
				from = (InternetAddress) message.getFrom()[0];
			} catch (MessagingException e) {
				log.warn(new Date() + ": invalid address.. skipping message");
				continue;
			}
			ret.setUsername(from.getAddress());
			if (part.equalsIgnoreCase(configuration
					.getProperty("import.subjectReceiptPrefix")))
				continue;
			location.concat(" " + part);
		}
		ret.setLocation(location);
		return ret;
	}

	private static long getLatestReceiptForUser(String user)
			throws SQLException {
		String stmt = configuration.getProperty("import.lastReceiptForUser");
		Connection conn = DriverManager.getConnection(configuration
				.getProperty("jdbc.url"));
		PreparedStatement prepped = conn.prepareStatement(stmt);
		prepped.setString(1, user);
		prepped.execute();
		ResultSet results = prepped.getResultSet();
		results.next();
		long ret = results.getLong(1);
		conn.close();
		return ret;
	}

	private static void newTransactionFromPaypal(Message message)
			throws MessagingException {
		Multipart content = null;
		try {
			content = (Multipart)message.getContent();
		} catch (IOException e) {
			// according to http://stackoverflow.com/a/470492/783412, set the
			// logger to log these events
			System.setProperty("java.util.logging.ConsoleHandler.level",
					"FINER");
			log.error("{}", e.getMessage(), e);
			// according to
			// http://stackoverflow.com/questions/4970513/java-logging-levels-confusion,
			// the default logging level is INFO
			System.setProperty("java.util.logging.ConsoleHandler.level", "INFO");
		}
		Pattern pattern = Pattern
				.compile("You sent a payment of =24([0-9.]+) USD to ([^=])+");
		BufferedReader bodyReader = null;
		try {
			bodyReader = new BufferedReader(new InputStreamReader(content.getBodyPart(0).getInputStream()));
		} catch (IOException e1) {
			// according to http://stackoverflow.com/a/470492/783412, set the logger to log these events
			System.setProperty("java.util.logging.ConsoleHandler.level","FINER");
			log.error("{}", e1.getMessage(), e1);	// according to http://stackoverflow.com/questions/4970513/java-logging-levels-confusion, the default logging level is INFO
			System.setProperty("java.util.logging.ConsoleHandler.level", "INFO");		
		}
		StringBuffer body = new StringBuffer();
		String line  = null;
		try {
			while ((line = bodyReader.readLine()) != null) {
				body.append(line);
			}
		} catch (IOException e) {
			// according to http://stackoverflow.com/a/470492/783412, set the logger to log these events
			System.setProperty("java.util.logging.ConsoleHandler.level","FINER");
			log.error("{}", e.getMessage(), e);
			// according to http://stackoverflow.com/questions/4970513/java-logging-levels-confusion, the default logging level is INFO
			System.setProperty("java.util.logging.ConsoleHandler.level", "INFO");		
		}
		Matcher matcher = pattern.matcher(body.toString());
		log.info("Amount should be " + matcher.group(1)
				+ " and location should be " + matcher.group(2));
		ReceiptBean receiptBean = new ReceiptBean();
		receiptBean.setAmount(Double.parseDouble(matcher.group(1)));
		receiptBean.setLocation(matcher.group(2).trim());
		receiptBean.setUsername("hd1");
		receiptBean.setDoneAt(message.getSentDate());
		receiptBean.save("Paypal");
	}

	private static void newTransactionFromRcpt(Message message)
			throws MessagingException {
		InternetAddress from = (InternetAddress) message.getFrom()[0];
		ReceiptBean r = new ReceiptBean();

		System.setProperty("us.d8u.finance.localUser", from.getAddress());
		r.setDoneAt(message.getSentDate());
		String[] subjectParts = message.getSubject().split(" ");
		String where = "";
		int j = 1;
		for (j = 1; j != subjectParts.length - 1; j++) {
			where += subjectParts[j] + " ";
		}
		r.setLocation(where.trim());
		try {
			r.setAmount(Double.parseDouble(subjectParts[j]));
		} catch (NumberFormatException e1) {
			NumberFormat currency = NumberFormat.getCurrencyInstance();
			try {
				currency.parseObject(subjectParts[j]);
			} catch (ParseException p) {
				throw new RuntimeException(p.getLocalizedMessage());
			}
		}
		r.setUsername(System.getProperty("username"));
		r.save("us.d8u.finance.ImportFromMail");
	}

	private static String UserForEmail(String emailAddress) throws SQLException {
		String sql = configuration.getProperty("import.userForEmail");
		Connection conn = DriverManager.getConnection(configuration
				.getProperty("jdbc.url"));
		PreparedStatement prep = conn.prepareStatement(sql);
		prep.setString(1, emailAddress);
		ResultSet results = prep.executeQuery();
		results.next();
		String ret = results.getString(1);
		conn.close();
		return ret;
	}

	private static void export(Message sourceMessage) throws MessagingException {
		long maxAddTime = -1L;
		try {
			maxAddTime = getLatestReceiptForUser(System.getProperty("username"));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		if (sourceMessage.getSentDate().getTime() < maxAddTime) {
			return;
		}
		String subject = sourceMessage.getSubject();
		InternetAddress from = (InternetAddress) sourceMessage.getFrom()[0];
		String[] subjectWords = subject.split(" ");
		System.setProperty("format", subjectWords[1]);
		String user = null;
		try {
			user = UserForEmail(from.getAddress());
		} catch (SQLException e) {
			log.warn(new Date() + " " + e.getLocalizedMessage());
		}
		StringWriter bodySrc = null;
		try {
			bodySrc = (StringWriter) Export.export(user);
		} catch (SQLException e) {
			log.warn(new Date() + " " + e.getLocalizedMessage());
		}

		// from
		// http://www.mkyong.com/java/javamail-api-sending-email-via-gmail-smtp-example/
		// -- TLS example
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("hd1@jsc.d8u.us", System
						.getProperty("password"));
			}
		});

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress("hd1@jsc.d8u.us"));
		message.setRecipient(Message.RecipientType.TO, from);
		message.setSubject("Transactions in " + System.getProperty("format")
				+ " enclosed");
		message.setText(bodySrc.toString());

		Transport.send(message);
	}

	public static void main(String args[]) {
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");
		Session session = Session.getDefaultInstance(props, null);
		Store store = null;
		try {
			store = session.getStore("imaps");
		} catch (NoSuchProviderException e) {
			log.warn(new Date() + " " + e.getLocalizedMessage());
			System.exit(e.hashCode());
		}
		try {
			store.connect("imap.gmail.com", "hd1@jsc.d8u.us",
					System.getProperty("password"));
		} catch (MessagingException e) {
			log.warn(new Date() + " " + e.getLocalizedMessage());
			System.exit(e.hashCode());
		}

		Folder inbox = null;
		try {
			inbox = store.getFolder("INBOX");
		} catch (MessagingException e) {
			log.warn(new Date() + " " + e.getLocalizedMessage());
			System.exit(e.hashCode());
		}
		try {
			inbox.open(Folder.READ_WRITE);
		} catch (MessagingException e) {
			log.warn(new Date() + " " + e.getLocalizedMessage());
			System.exit(e.hashCode());
		}

		Message messages[] = null;
		try {
			messages = inbox.getMessages();
		} catch (MessagingException e) {
			log.warn(new Date() + " " + e.getLocalizedMessage());
			System.exit(e.hashCode());
		}
		for (int i = 0; i != messages.length; i++) {
			Message message = messages[i];
			try {
				if (message.getFrom().equals(
						InternetAddress.parse("service@paypal.com"))) {
					newTransactionFromPaypal(message);
				}
				if (message.getSubject().toUpperCase().startsWith("RCPT")) {
					newTransactionFromRcpt(message);
				} else if (message.getSubject().toUpperCase()
						.startsWith("RQST:")) {
					export(message);
				}
			} catch (MessagingException e) {
				log.info(ImportFromMail.class.getCanonicalName(), Thread
						.currentThread().getStackTrace()[1].getMethodName(), e);

			}
		}

		try {
			inbox.close(true);
		} catch (MessagingException e1) {
			e1.printStackTrace(System.err);
		}
		try {
			store.close();
		} catch (MessagingException e) {
			e.printStackTrace(System.err);
		}
		try {
			loadConfig();
			Connection conn = DriverManager.getConnection(configuration
					.getProperty("jdbc.url"));
			conn.setAutoCommit(false);
			// http://stackoverflow.com/a/8244823/783412
			conn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE).executeUpdate(
					configuration.getProperty("import.cleanupOld"));
			conn.commit();
			conn.close();
		} catch (SQLException e2) {
			log.info(ImportFromMail.class.getCanonicalName(), Thread
					.currentThread().getStackTrace()[1].getMethodName(), e2);
		}
	}
}
