package us.d8u.finance;
public interface ImportFrom {
  public ReceiptBean toReceiptBean();
}
