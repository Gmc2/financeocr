/**
 * 
 */
package us.d8u.finance;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author hdiwan
 * restoring the ant setup target as a Java class
 */
public class Install {
	public static void main (String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException  {
		Class.forName("org.h2.Driver").newInstance();
		Connection conn = null;
		try {
			conn = DriverManager.getConnection("jdbc:h2:tcp://192.168.1.6/finance;USER=sa");
		} catch (SQLException e) {
		}
		try {
			conn.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace(System.err);
		}
		FileReader reader_ = null;
		try {
			reader_ = new FileReader("src/main/resources/install.sql");
		} catch (FileNotFoundException e) {
			e.printStackTrace(System.err);
			System.exit(e.hashCode()*-1);
		}
		BufferedReader reader = new BufferedReader(reader_);
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				System.err.println(line);
				try {
					conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE).executeUpdate(line);
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
		try {
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace(System.err);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace(System.err);
		}
		try {
			reader.close();
		} catch (IOException e1) {
			e1.printStackTrace(System.err);
			System.exit(-1);
		}
		try {
			reader_.close();
		} catch (IOException e) {
			e.printStackTrace(System.err);
			System.exit(-1);
		}
	}
}
