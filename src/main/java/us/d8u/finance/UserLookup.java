package us.d8u.finance;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * UserLookup -  returns a list of valid users, one line at a time
 */
public class UserLookup {
	public static void main (String[] args) throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:h2:tcp://localhost/finance", "sa", null);
		ResultSet users = conn.createStatement().executeQuery("select name from app_users");
		do {
			users.next();
			System.out.println(users.getString(1));
		} while (users.isLast() == false);
		conn.close();
	}
}
