/**
 * @author Hasan Diwan <hd1@jsc.d8u.us>
 */
package us.d8u.finance;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import twitter4j.DirectMessage;
import twitter4j.Friendship;
import twitter4j.Paging;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class ImportFromTwitter implements ImportFrom, Runnable {
	private static final Logger log = LoggerFactory
			.getLogger("us.d8u.finance.ImportFromTwitter");
	private static final String[] TWITTER_SCREENNAME = new String[] { "FinanceOCR" };
	private final Properties config = new Properties();
	// do so using direct messages
	private DirectMessage message;

	private Set<String> goodTwitterHandles = new HashSet<String>();

	public ReceiptBean toReceiptBean() {
		ReceiptBean ret = new ReceiptBean();
		String[] words = message.getText().split(" ");
		String amount = words[words.length - 1];
		Double money = Double.parseDouble(amount);
		String location = "";
		for (int i = 1; i != words.length; i++) {
			location += words[i] + " ";
		}
		location = location.trim();
		ret.setLocation(location);
		ret.setAmount(money);
		ret.setDoneAt(message.getCreatedAt());
		ret.setUsername(this.lookupUserByTwitterHandle(System
				.getProperty("us.d8u.finance.twitterHandle")));
		return ret;
	}

	public void importFromTwitter() {
		Twitter twitter = TwitterFactory.getSingleton();
		Paging paging = new Paging(1);
		try {
			config.load(this.getClass().getClassLoader().getResourceAsStream("FinanceOCR.properties"));
		} catch (IOException e) {
			log.warn(e.getLocalizedMessage(), e);
		}
		List<DirectMessage> messages = null;
		try {
			messages = twitter.getDirectMessages(paging);
		} catch (TwitterException e) {
			log.warn(e.getLocalizedMessage(), e);
		}
		for (DirectMessage message : messages) {
			if (goodTwitterHandles.contains(message.getSenderScreenName()) == false)
				continue;
			String text = message.getText();
			log.info("running twitter import with message \"" + text + "\"");
			System.setProperty("us.d8u.finance.twitterHandle",
					message.getSenderScreenName());
			ReceiptBean receipt = this.toReceiptBean();
			log.info("About to save -- "+receipt.toString());
			receipt.save(this.getClass().getCanonicalName());
		}
	}

	/**
	 * @param senderScreenName
	 * @return
	 */
	private String lookupUserByTwitterHandle(String senderScreenName) {
		String userId = "-1";
		String sql = "select user_id from user_twitter t join app_users u on t.user_id = u.id where t.name = ?";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(config.getProperty("jdbc.url"));
		} catch (SQLException e) {
			log.info(this.getClass().getCanonicalName(), Thread.currentThread()
					.getStackTrace()[1].getMethodName(), e);
		}
		PreparedStatement prepped = null;
		try {
			prepped = conn.prepareStatement(sql);
		} catch (SQLException e) {
			log.info(this.getClass().getCanonicalName(), Thread.currentThread()
					.getStackTrace()[1].getMethodName(), e);
		}
		try {
			prepped.setString(1, senderScreenName);
		} catch (SQLException e) {
			log.info(this.getClass().getCanonicalName(), Thread.currentThread()
					.getStackTrace()[1].getMethodName(), e);
		}
		ResultSet results = null;
		try {
			results = prepped.executeQuery();
		} catch (SQLException e) {
			log.info(this.getClass().getCanonicalName(), Thread.currentThread()
					.getStackTrace()[1].getMethodName(), e);
		}
		try {
			results.next();
		} catch (SQLException e) {
			log.info(this.getClass().getCanonicalName(), Thread.currentThread()
					.getStackTrace()[1].getMethodName(), e);
		}
		try {
			userId = results.getString(1);
		} catch (SQLException e) {
			log.info(this.getClass().getCanonicalName(), Thread.currentThread()
					.getStackTrace()[1].getMethodName(), e);
		}
		return userId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		this.importFromTwitter();
		this.updateFollowers();
	}

	/**
	 * 
	 */
	private void updateFollowers() {
		this.goodTwitterHandles = new HashSet<String>();
		List<Friendship> friends = null;
		try {
			friends = TwitterFactory.getSingleton().lookupFriendships(
					TWITTER_SCREENNAME);
		} catch (TwitterException e) {
			e.printStackTrace(System.err);
			System.exit(-1);
		}
		for (Friendship friend : friends) {
			this.goodTwitterHandles.add(friend.getScreenName());
			log.debug("running twitter friends update");
		}
	}
}
