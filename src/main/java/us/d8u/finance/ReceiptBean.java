package us.d8u.finance;

import java.io.InputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReceiptBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(ReceiptBean.class);
	private String username;
	private Long doneAt;
	private String location;
	private Double amount;

	public ReceiptBean() {
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String user) {
		username = user;
	}

	public void setDoneAt(long date) {
		doneAt = date;
	}

	public void setDoneAt(Date date) {
		doneAt = date.getTime();
	}

  public Long getDoneAt() {
    return this.doneAt; 
  }

	public String getLocation() {
		return location;
	}

	public void setLocation(String store) {
		location = store;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double anAmount) {
		amount = anAmount;
	}

	public String toString() {
		return "{" + location + ", " + username + ", "
			+ new Date(doneAt * 1000) + ", " + amount + "}";
	}

	public void save(String source) {
		PreparedStatement prepped = null;
		Connection conn = null;
		ResultSet userInformation = null;
		InputStream configStream = this.getClass().getClassLoader().getResourceAsStream("FinanceOCR.properties");
		Properties config = new Properties();
		try {
			config.load(configStream);
		} catch (IOException e) {
			log.warn(e.getLocalizedMessage(), e);
		}
		try {
			conn = DriverManager.getConnection(config.getProperty("jdbc.url"));
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e );
			System.exit(1);
		}
		try {
			conn.setAutoCommit(false);
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e );
		}
		try {
			prepped = conn.prepareStatement(
			                                "select user_id from user_emails where email = ?",
			                                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
		try {
			prepped.setString(1, System.getProperty("us.d8u.finance.localUser"));
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
		try {
			userInformation = prepped.executeQuery();
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
		try {
			userInformation.next();
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
		int userId = -1;
		try {
			userId = userInformation.getInt(1);
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
		try {
			prepped = conn
				.prepareStatement(
				                  "insert into receipts (location, amount, done_on, source, user_id) values (?,?,?,?,?);",
				                  ResultSet.TYPE_SCROLL_INSENSITIVE,
				                  ResultSet.CONCUR_UPDATABLE);
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
		try {
			prepped.setString(1, StringUtils.capitalize(this.getLocation()));
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
		try {
			prepped.setDouble(2, this.getAmount());
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
		try {
			prepped.setLong(3, this.getDoneAt());
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
		try {
			prepped.setString(4, source);
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
		try {
			prepped.setInt(5, userId);
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
		try {
			prepped.executeUpdate();
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
		try {
			conn.commit();
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
		try {
			conn.close();
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage(),e);
		}
	}
}
