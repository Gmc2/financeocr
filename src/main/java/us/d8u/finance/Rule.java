package us.d8u.finance;

public interface Rule {
	/**
	 * Composition of Rules
	 */

	/** 
	 * categoryFor
	 * @param text the text that has to occur for this to be classified in the category
	 */
	public int categoryFor(String text);
}


