create table app_users (id bigint primary key auto_increment, name varchar(128) not null, timezone text, language char(2));
create table user_emails(id bigint primary key auto_increment, user_id int not null, email varchar(128) not null);
create table user_twitter(user_id bigint not null, twitter text not null);
create table receipts(id bigint primary key auto_increment, user_id int, location varchar(512) not null, amount decimal(8,2) not null, done_on long not null, source varchar(255) not null, added_at timestamp default now());
-- insert statements for app_users
insert into app_users (name, timezone, language) values ('hd1', 'EST5EDT', 'en'), ('Proshot', 'Europe/Amsterdam', 'nl'), ('ray_diwan', 'EST5EDT', 'en');
-- insert statements for twitter accounts
insert into user_twitter(user_id, twitter) values (2, 'rforfinance'), (1, 'hdiwan');
-- insert statements for user_emails
insert into user_emails(user_id, email) values(1, 'hasan.diwan@gmail.com'), (2, 'w.erselina@gmail.com'), (3, 'rauf.diwan@gmail.com');
