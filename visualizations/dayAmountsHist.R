#!Rscript
# Which day do I spend the most money?
require(ggplot2)
location <- paste(sep='/', '~/public_html/Finance', commandArgs(trailingOnly=TRUE), 'txns.csv')
rcpts <- read.csv(location, header=TRUE)
rcpts$DONE_ON <- as.POSIXct(as.numeric(rcpts[,1]), origin='1969-12-31 16:00:00.000')
rcpts$DAY <- format(rcpts$DONE_ON, format='%A')
rcpts <- rcpts[rcpts$Amount> 0,]
daySpending <- aggregate(rcpts$Amount, list(day=rcpts$DAY), sum)
daySpending <- data.frame(Day.of.week = daySpending[,1], Amount = as.numeric(daySpending[,2]))
dir.create(paste('/home/hdiwan/public_html/Finance/',commandArgs(trailingOnly=TRUE)[1], sep=''))
ggplot(daySpending, aes(x=Day.of.week, y=Amount))+geom_histogram()+theme_bw()+opts(title='Day of the week vs. Spending Amount')
ggsave(path.expand(paste('~/public_html/Finance', sep='/', commandArgs(trailingOnly=TRUE)[1], 'dayAmountsHist.png')))
