# divides it into the current month

if __name__ == '__main__':
    from java.text import NumberFormat
    currencyFormat = NumberFormat.getCurrencyInstance()
    from java.sql import DriverManager
    conn = DriverManager.getConnection(u'jdbc:h2:tcp://192.168.1.6/finance;USER=sa;IFEXISTS=true')
    import sys
    line = conn.createStatement().executeQuery(u"select location, amount, done_on from receipts as r join app_users as a on r.user_id = a.id where amount > 0 and name = '%s'" % sys.argv[1])
    while not line.isLast():
        line.next()
        amount = currencyFormat.format(line.getDouble(u'amount'))

        from org.joda.time import DateTime
        dt = DateTime(line.getLong(u'done_on'))

        startDate = DateTime().withDayOfMonth(1)
        length_of_month = startDate.dayOfMonth().getMaximumValue()
        endDate = startDate.withDayOfMonth(length_of_month)
        from org.joda.time import Interval
        interval = Interval(startDate, endDate)
        if not interval.contains(dt):
            continue
        print (u'%s, %s, %s, %s' %(sys.argv[1], line.getString(u'location'), amount, dt))
    conn.close()
