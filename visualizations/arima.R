#!Rscript --vanilla
# ARIMA model of my spending
require(forecast)
require(xts)
require(ggplot2)
rcpts <- read.csv(paste('~/public_html/Finance',commandArgs(trailingOnly=TRUE)[1], 'txns.csv',sep='/'), header=TRUE)
rcpts <- rcpts[rcpts$Amount > 0,]
fit <- auto.arima(rcpts$Amount)
png(paste('/home/hdiwan/public_html/Finance', commandArgs(trailingOnly=TRUE)[1], 'arima.png', sep='/'), width=par('din')[1], height=par('din')[2], units='in', res=300)
plot(forecast(fit))
dev.off()
