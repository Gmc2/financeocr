#!/bin/sh
CLASSPATH=$HOME/bin/FinanceOCR-jar-with-dependencies.jar
export CLASSPATH

if [ "$#" -eq "2" ]
then
  /usr/bin/java -Djdbc.drivers=org.h2.Driver -Dpassword=$2 -Dusername=$1 us.d8u.finance.Driver -i
  /usr/bin/java -Djdbc.drivers=org.h2.Driver us.d8u.finance.Driver -d
elif [ "$#" -eq "1" ]
then
  /bin/rm -Rf $HOME/public_html/Finance/$1
  /bin/mkdir -p $HOME/public_html/Finance/$1
  /usr/bin/java -Djava.io.tmpdir=/var/tmp -Djdbc.drivers=org.h2.Driver -Dformat=csv -Dusername=$1 us.d8u.finance.Driver -e > $HOME/public_html/Finance/$1/txns.csv
  /bin/cat $HOME/public_html/Finance/$1/txns.csv | /bin/bzip2 -9c $HOME/public_html/Finance/$1/txns.csv > $HOME/public_html/Finance/$1/txns.csv.bz2
  /usr/bin/java -Djava.io.tmpdir=/var/tmp -Djdbc.drivers=org.h2.Driver -Dformat=xml -Dusername=$1 us.d8u.finance.Driver -e > $HOME/public_html/Finance/$1/txns.xml
  /bin/cat $HOME/public_html/Finance/$1/txns.xml | /bin/bzip2 -9c $HOME/public_html/Finance/$1/txns.xml > $HOME/public_html/Finance/$1/txns.xml.bz2
  /usr/bin/java -Djava.io.tmpdir=/var/tmp -Djdbc.drivers=org.h2.Driver -Dformat=json -Dusername=$1 us.d8u.finance.Driver -e > $HOME/public_html/Finance/$1/txns.json
  /bin/cat $HOME/public_html/Finance/$1/txns.json | /bin/bzip2 -9c $HOME/public_html/Finance/$1/txns.json > $HOME/public_html/Finance/$1/txns.json.bz2
  /usr/bin/java -Djava.io.tmpdir=/var/tmp -Djdbc.drivers=org.h2.Driver -Dformat=xls -Dusername=$1 us.d8u.finance.Driver -e > /dev/null
  /usr/bin/find $HOME/workspace/FinanceOCR/visualizations/ -name '*R' -exec /bin/sh $HOME/workspace/FinanceOCR/visualize.sh {} $1 \; 
  rm -f Rplots.pdf
  /usr/bin/perl $HOME/workspace/FinanceOCR/generateIndex.pl $1
  /usr/bin/rsync -az $HOME/public_html/Finance/* 192.168.1.3:public_html/visualisations
else
  echo 1>&2 "$0: Local username and hd1@jsc.d8u.us' password are required!"
fi
